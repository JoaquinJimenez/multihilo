# Resumen

Proyecto para la asignatura PSP del ciclo de DAM, en el cual se debian utilizar diferentes hilos

## Definición del proyecto

La aplicación debe manejar varios hilos en todo momento, pero no tienen porque tener una funcionalidad lógica. En este caso se ha realizado una simulación parcial de una gestión de un frente bélico.

# Implementación de los hilos

1. Trabajo cooperativo de varios hilos.

Un almacén con una capacidad x es suministrado por varias fábricas que se reparten la fabricación de munición que le ha indicado el usuario hasta llenar el almacén.

2. Sincronización de varios hilos usando el método join().

Un quirófano solo puede atender a una persona, por lo que el resto tendran que esperar hasta que termine la operación del que le precede.

3. Acceder de forma atómica a una parte del código.

Un avión despega y apoya a cada a un frente de batalla en cada incursión, el resto de frentes tendrán que esperar a que el avión regrese.

4. Productor consumidor para el acceso a un solo recurso compartido por varios hilos.

En un almacen estan guardadas balas. Los frentes consumen estas balas mientras que las fabricas las producen.

